package br.com.db1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootValidacaoSenhadApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootValidacaoSenhadApplication.class, args);
	}
}