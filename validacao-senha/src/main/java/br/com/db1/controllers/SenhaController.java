package br.com.db1.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.db1.service.ValidacaoSenhaService;

@Controller
public class SenhaController {

	@RequestMapping("/index.html")
	public ModelAndView firstPage() {
		return new ModelAndView("index");
	}
	
	@PostMapping("/validacao-senha")
    @ResponseBody
    public int sayHello(@RequestParam(name="senha") Object senha) {
		System.out.println("Senha " + senha);
        return new ValidacaoSenhaService().doCalTotalPontos(senha.toString());
    }


}
