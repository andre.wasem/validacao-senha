package br.com.db1.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.db1.message.Response;
import br.com.db1.message.Senha;
import br.com.db1.service.ValidacaoSenhaService;

@RestController
@RequestMapping("/api/senha")
public class Valida {
	
	@PostMapping(value = "/valida")
	public Response postValida(@RequestBody Senha senha) {
		
		Integer totalPontos = new ValidacaoSenhaService().doCalTotalPontos(senha.getSenha());
		
		Response response = new Response("Done", totalPontos);
		return response;
	}
}
