package br.com.db1.service;

import br.com.db1.utils.SenhaConstantes;

public class SenhaLetrasMinusculasService {

	// atributos para contagem de letras maisculas
	private int contLetrasMinusculas = 0;
	private int contLetrasMinusculasTemp = -1;
	private int contLetrasMinusculasConsecutivas = 0;
	
	// atributo para contagem de letras, minusculas e maisculas
	private int contLetrasSequencial;
	
	public void doAdicaoPontos(String[] arrSenha, int index) {
		if (getContLetrasMinusculasTemp() > -1) {
			if ((getContLetrasMinusculasTemp() + 1) == index) {
				setContLetrasMinusculasConsecutivas(getContLetrasMinusculasConsecutivas() + 1);
			}
		}
		setContLetrasMinusculasTemp(index);
		setContLetrasMinusculas(getContLetrasMinusculas() + 1);
	}
	
	public void doContSeqStrings(String pwd) {
		for (int i = 0; i < 23; i++) {
			String strSenha = SenhaConstantes.LETRAS.substring(i, i + 3);
			String sRev = new StringBuilder().append(strSenha).reverse().toString();
			if (pwd.toLowerCase().indexOf(strSenha) != -1 || pwd.toLowerCase().indexOf(sRev) != -1) {
				setContLetrasSequencial(getContLetrasSequencial() + 1);
			}
		}
	}
	
	public int doSubPontos(String senha, SenhaSimbolosService senhaSimbolos, SenhaNumerosService senhaNumeros, SenhaLetrasMaisculasService senhaLetrasMaisculas, int totalPontos) {
		
		int totalCarNaoLetras = senhaSimbolos.getContSimbolos() + senhaNumeros.getContNumeros();
		
		// apenas letras
		if ((getContLetrasMinusculas() > 0 || senhaLetrasMaisculas.getContLetrasMaisculas() > 0) && totalCarNaoLetras == 0) {
			totalPontos = totalPontos - senha.length();
		}
		
		// letras minusculas consecutivas
		if (getContLetrasMinusculasConsecutivas() > 0) {
			totalPontos = totalPontos - (getContLetrasMinusculasConsecutivas() * 2);
		}
		
		// Letras sequenciais
		if (getContLetrasSequencial() > 0) {
			totalPontos = totalPontos - (getContLetrasSequencial() * 3);
		}
		
		return totalPontos;
	}
	
	public int doMultiplicacaoPontos(String senha, int totalPontos) {
		
		if (getContLetrasMinusculas() > 0 && getContLetrasMinusculas() < senha.length()) {
			totalPontos = totalPontos + ((senha.length() - getContLetrasMinusculas()) * 2);
		}
		
		return totalPontos;
	}
	
	public int getContLetrasMinusculas() {
		return contLetrasMinusculas;
	}

	public void setContLetrasMinusculas(int contLetrasMinusculas) {
		this.contLetrasMinusculas = contLetrasMinusculas;
	}

	public int getContLetrasMinusculasTemp() {
		return contLetrasMinusculasTemp;
	}

	public void setContLetrasMinusculasTemp(int contLetrasMinusculasTemp) {
		this.contLetrasMinusculasTemp = contLetrasMinusculasTemp;
	}

	public int getContLetrasMinusculasConsecutivas() {
		return contLetrasMinusculasConsecutivas;
	}

	public void setContLetrasMinusculasConsecutivas(int contLetrasMinusculasConsecutivas) {
		this.contLetrasMinusculasConsecutivas = contLetrasMinusculasConsecutivas;
	}
	
	public int getContLetrasSequencial() {
		return contLetrasSequencial;
	}

	public void setContLetrasSequencial(int contLetrasSequencial) {
		this.contLetrasSequencial = contLetrasSequencial;
	}

}
