package br.com.db1.service;

import br.com.db1.utils.SenhaConstantes;

public class SenhaNumerosService {

	// atributos para contagem de n�meros
	private int contNumeros = 0;
	private int contNumerosTemp = -1;
	private int contNumerosConsecutivos;
	private int contNumerosSequencial;
	
	public void doAdicaoPontos(String[] arrSenha, int index) {
		if (getContNumerosTemp() > -1) {
			if ((getContNumerosTemp() + 1) == index) {
				setContNumerosConsecutivos(getContNumerosConsecutivos() + 1);
			}
		}
		setContNumerosTemp(index);
		setContNumeros(getContNumeros() + 1);
	}
	
	public void doContSeqNum(String senha) {
		for (int index = 0; index < 8; index++) {
			String strSenha = SenhaConstantes.NUMEROS.substring(index, index + 3);
			String strSenhaReversa = new StringBuilder().append(strSenha).reverse().toString();
			if (senha.toLowerCase().indexOf(strSenha) != -1 || senha.toLowerCase().indexOf(strSenhaReversa) != -1) {
				setContNumerosSequencial(getContNumerosSequencial() + 1);
			}
		}
	}
	
	public int doSubPontos(String senha, SenhaLetrasMinusculasService senhaLetrasMinusculas, SenhaLetrasMaisculasService senhaLetrasMaisculas, SenhaSimbolosService senhaSimbolos, int totalPontos) {
		int totalCarNaoNumericos = senhaLetrasMinusculas.getContLetrasMinusculas() + senhaLetrasMaisculas.getContLetrasMaisculas() + senhaSimbolos.getContSimbolos();

		// apenas n�meros
		if (totalCarNaoNumericos == 0 && getContNumeros() > 0) {
			totalPontos = totalPontos - senha.length();
		}
		
		// numeros consecutivos
		if (getContNumerosConsecutivos() > 0) {
			totalPontos = totalPontos - (getContNumerosConsecutivos() * 2);
		}

		// N�meros sequenciais
		if (getContNumerosSequencial() > 0) {
			totalPontos = totalPontos - (getContNumerosSequencial() * 3);
		}
		
		return totalPontos;
	}
	
	public int doMultiplicacaoPontos(String senha, int totalPontos) {
		if (getContNumeros() > 0 && getContNumeros() < senha.length()) {
			totalPontos = totalPontos + (getContNumeros() * 4);
		}
		return totalPontos;
	}

	public int getContNumeros() {
		return contNumeros;
	}

	public void setContNumeros(int contNumeros) {
		this.contNumeros = contNumeros;
	}

	public int getContNumerosTemp() {
		return contNumerosTemp;
	}

	public void setContNumerosTemp(int contNumerosTemp) {
		this.contNumerosTemp = contNumerosTemp;
	}

	public int getContNumerosConsecutivos() {
		return contNumerosConsecutivos;
	}

	public void setContNumerosConsecutivos(int contNumerosConsecutivos) {
		this.contNumerosConsecutivos = contNumerosConsecutivos;
	}

	public int getContNumerosSequencial() {
		return contNumerosSequencial;
	}

	public void setContNumerosSequencial(int contNumerosSequencial) {
		this.contNumerosSequencial = contNumerosSequencial;
	}
}