package br.com.db1.service;

public class SenhaRepetidosRequiridosService {

	private int contCaracteresRequeridos;
	private int contCaracteresMinimosRequiridos;

	private int contMediaInclusoesRepetidas;
	private int contCaracteresRepeditos;

	private int contMediaCaracteresRequiridos = 0;

	private int contCaracteresUnicos;

	public int getContCaracteresRequeridos() {
		return contCaracteresRequeridos;
	}

	public void setContCaracteresRequeridos(int contCaracteresRequeridos) {
		this.contCaracteresRequeridos = contCaracteresRequeridos;
	}

	public int getContCaracteresMinimosRequiridos() {
		return contCaracteresMinimosRequiridos;
	}

	public void setContCaracteresMinimosRequiridos(int contCaracteresMinimosRequiridos) {
		this.contCaracteresMinimosRequiridos = contCaracteresMinimosRequiridos;
	}

	public int getContMediaInclusoesRepetidas() {
		return contMediaInclusoesRepetidas;
	}

	public void setContMediaInclusoesRepetidas(int contMediaInclusoesRepetidas) {
		this.contMediaInclusoesRepetidas = contMediaInclusoesRepetidas;
	}

	public int getContCaracteresRepeditos() {
		return contCaracteresRepeditos;
	}

	public void setContCaracteresRepeditos(int contCaracteresRepeditos) {
		this.contCaracteresRepeditos = contCaracteresRepeditos;
	}

	public int getContMediaCaracteresRequiridos() {
		return contMediaCaracteresRequiridos;
	}

	public void setContMediaCaracteresRequiridos(int contMediaCaracteresRequiridos) {
		this.contMediaCaracteresRequiridos = contMediaCaracteresRequiridos;
	}

	public int getContCaracteresUnicos() {
		return contCaracteresUnicos;
	}

	public void setContCaracteresUnicos(int contCaracteresUnicos) {
		this.contCaracteresUnicos = contCaracteresUnicos;
	}
}
