package br.com.db1.service;

import br.com.db1.utils.SenhaConstantes;

public class SenhaSimbolosService {

	// atributos para contagem de simbolos
	private int contSimbolos = 0;
	private int contSimbolosTemp = -1;
	private int contSimbolosSequencial;
	
	public void doAdicaoPontos(String[] arrSenha, int index) {
		if (getContSimbolosTemp() > -1) {
			if ((getContSimbolosTemp() + 1) == index) {
			}
		}
		setContSimbolosTemp(index);
		setContSimbolos(getContSimbolos() + 1);
	}
	
	public void doContSeqSimbolos(String senha) {
		for (int s = 0; s < 8; s++) {
			String strSenha = SenhaConstantes.SIMBOLOS.substring(s, s + 3);
			String strSenhaReversa = new StringBuilder().append(strSenha).reverse().toString();
			if (senha.toLowerCase().indexOf(strSenha) != -1 || senha.toLowerCase().indexOf(strSenhaReversa) != -1) {
				setContSimbolosSequencial(getContSimbolosSequencial() + 1);
			}
		}
	}
	
	public int doSubPontos(String senha, int totalPontos) {
		// Simbolos sequenciais
		if (getContSimbolosSequencial() > 0) {
			totalPontos = totalPontos - (getContSimbolosSequencial() * 3);
		}
		return totalPontos;
	}
	
	public int doMultiplicacaoPontos(String senha, int totalPontos) {
		if (getContSimbolos() > 0) {
			totalPontos = totalPontos + (getContSimbolos() * 6);
		}
		
		return totalPontos;
	}

	public int getContSimbolos() {
		return contSimbolos;
	}

	public void setContSimbolos(int contSimbolos) {
		this.contSimbolos = contSimbolos;
	}

	public int getContSimbolosTemp() {
		return contSimbolosTemp;
	}

	public void setContSimbolosTemp(int contSimbolosTemp) {
		this.contSimbolosTemp = contSimbolosTemp;
	}

	public int getContSimbolosSequencial() {
		return contSimbolosSequencial;
	}

	public void setContSimbolosSequencial(int contSimbolosSequencial) {
		this.contSimbolosSequencial = contSimbolosSequencial;
	}
}
