package br.com.db1.service;

public class SenhaLetrasMaisculasService {

	// atributos para contagem de letras minusculas
	private int contLetrasMaisculas = 0;
	private int contLetrasMaisculasTemp = -1;
	private int contLetrasMaisculasConsecutivas = 0;
	
	public void doAdicaoPontos(String[] arrSenha, int index) {
		if (getContLetrasMaisculasTemp() > -1) {
			if ((getContLetrasMaisculasTemp() + 1) == index) {
				setContLetrasMaisculasConsecutivas(getContLetrasMaisculasConsecutivas() + 1);
			}
		}
		setContLetrasMaisculasTemp(index);
		setContLetrasMaisculas(getContLetrasMaisculas() + 1);
	}
	
	public int doSubPontos(String senha, int totalPontos) {
		// letras maisculas consecutivas
		if (getContLetrasMaisculasConsecutivas() > 0) {
			totalPontos = totalPontos - (getContLetrasMaisculasConsecutivas() * 2);
		}
		return totalPontos;
	}
	
	public int doMultiplicacaoPontos(String senha, int totalPontos) {
		
		if (getContLetrasMaisculas() > 0 && getContLetrasMaisculas() < senha.length()) {
			totalPontos = totalPontos + ((senha.length() - getContLetrasMaisculas()) * 2);
		}
		
		return totalPontos;
	}

	public int getContLetrasMaisculas() {
		return contLetrasMaisculas;
	}

	public void setContLetrasMaisculas(int contLetrasMaisculas) {
		this.contLetrasMaisculas = contLetrasMaisculas;
	}

	public int getContLetrasMaisculasTemp() {
		return contLetrasMaisculasTemp;
	}

	public void setContLetrasMaisculasTemp(int contLetrasMaisculasTemp) {
		this.contLetrasMaisculasTemp = contLetrasMaisculasTemp;
	}

	public int getContLetrasMaisculasConsecutivas() {
		return contLetrasMaisculasConsecutivas;
	}

	public void setContLetrasMaisculasConsecutivas(int contLetrasMaisculasConsecutivas) {
		this.contLetrasMaisculasConsecutivas = contLetrasMaisculasConsecutivas;
	}
}