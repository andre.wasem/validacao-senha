package br.com.db1.service;

import br.com.db1.utils.SenhaConstantes;
import br.com.db1.utils.SenhaUtils;

public class ValidacaoSenhaService {

	private int totalPontos = 0;

	private String senha;
	
	private String[] arrSenha;
	
	private SenhaLetrasMaisculasService senhaLetrasMaisculas;
	
	private SenhaLetrasMinusculasService senhaLetrasMinusculas;
	
	private SenhaNumerosService senhaNumeros;
	
	private SenhaSimbolosService senhaSimbolos;
	
	private SenhaRepetidosRequiridosService senhaRepetidosRequiridos;
	
	public ValidacaoSenhaService() {
		senhaNumeros = new SenhaNumerosService();
		senhaSimbolos = new SenhaSimbolosService();
		senhaLetrasMaisculas = new SenhaLetrasMaisculasService();
		senhaLetrasMinusculas = new SenhaLetrasMinusculasService();
		senhaRepetidosRequiridos = new SenhaRepetidosRequiridosService();
	}
	
	/**
	 * Realiza c�lculo de pontos para a senha informada
	 * 
	 * @param senha
	 * @return totalPontos
	 */
	public int doCalTotalPontos(String senha) {
		this.senha = senha;
		
		// necessário realizar este cálculo inicial para contagem de pontos positivos
		this.totalPontos = senha.length() * 4;
		
		this.arrSenha = SenhaUtils.splitSenha(senha);

		doAdicaoPontos();

		doContSeqCaracteres();

		doMultiplicacaoPontos();

		doSubPontos();

		contCaracteresRequiridos();

		if (this.totalPontos > 100) {
			this.totalPontos = 100;
		} else if (this.totalPontos < 0) {
			this.totalPontos = 0;
		}

		return this.totalPontos;
	}
	
	/*
	 * realiza contagem de sequencias de caracteres de acordo com o tipo informado
	 */
	private void doContSeqCaracteres() {
		this.senhaLetrasMinusculas.doContSeqStrings(this.senha);

		this.senhaNumeros.doContSeqNum(this.senha);

		this.senhaSimbolos.doContSeqSimbolos(this.senha);
	}

	/*
	 * Para cada tipo de caracter existe um minimo requirido, 
	 * sendo assim este m�todo realiza a contagem de acordo com o tipo de 
	 * caracter informado e a quantidade minima informada para cada tipo
	 */
	private void contCaracteresRequiridos() {
		int arrCaracteresSenha[] = new int[] {  this.senha.length(), this.senhaLetrasMaisculas.getContLetrasMaisculas(), 
				 this.senhaLetrasMinusculas.getContLetrasMinusculas(), this.senhaNumeros.getContNumeros(),
				 this.senhaSimbolos.getContSimbolos() };

		int valorMinimo = SenhaConstantes.MINIMO_CARACTERES_SENHA - 1;

		for (int i = 0; i < 5; i++) {
			boolean flag = arrCaracteresSenha[i] == (valorMinimo + 1) || arrCaracteresSenha[i] > (valorMinimo + 1);
			
			if (flag) {
				 this.senhaRepetidosRequiridos.setContCaracteresRequeridos(this.senhaRepetidosRequiridos.getContCaracteresRequeridos() + 1);
			}
			valorMinimo = 0;
		}
		
		 this.senhaRepetidosRequiridos.setContCaracteresMinimosRequiridos((this.senha.length() >= SenhaConstantes.MINIMO_CARACTERES_SENHA) ? 3 : 4);

		if (this.senhaRepetidosRequiridos.getContCaracteresRequeridos() > this.senhaRepetidosRequiridos.getContCaracteresMinimosRequiridos()) {
			this.totalPontos = this.totalPontos + (this.senhaRepetidosRequiridos.getContCaracteresRequeridos() * 2);
		}
	}

	/*
	 * realiza a subtra��i de valores de acordo com o tipo de informa��o passada para a senha
	 */
	private void doSubPontos() {
		
		// apenas letras
		this.totalPontos = this.senhaLetrasMinusculas.doSubPontos(this.senha, this.senhaSimbolos, this.senhaNumeros, this.senhaLetrasMaisculas, this.totalPontos);
		
		this.totalPontos = this.senhaNumeros.doSubPontos(this.senha, this.senhaLetrasMinusculas, this.senhaLetrasMaisculas, this.senhaSimbolos, this.totalPontos);
		
		// Caracteres existentes e mais de um lugar
		if (this.senhaRepetidosRequiridos.getContCaracteresRepeditos() > 0) {
			this.senhaRepetidosRequiridos.setContMediaInclusoesRepetidas(this.senhaRepetidosRequiridos.getContMediaInclusoesRepetidas() + 1);
			this.totalPontos = this.totalPontos - this.senhaRepetidosRequiridos.getContMediaInclusoesRepetidas();
		}
		
		this.totalPontos = this.senhaLetrasMaisculas.doSubPontos(this.senha, this.totalPontos);

		this.totalPontos = this.senhaSimbolos.doSubPontos(this.senha, this.totalPontos);
	}

	/*
	 * realiza as multiplica��es de acordo com o tipo de caracter encontrado na descri��o da senha
	 */
	private void doMultiplicacaoPontos() {

		if (this.senhaRepetidosRequiridos.getContMediaCaracteresRequiridos() > 0) {
			this.totalPontos = this.totalPontos + (this.senhaRepetidosRequiridos.getContMediaCaracteresRequiridos() * 2);
		}

		this.totalPontos = this.senhaSimbolos.doMultiplicacaoPontos(this.senha, this.totalPontos);
		
		this.totalPontos = this.senhaNumeros.doMultiplicacaoPontos(this.senha, this.totalPontos);
		
		this.totalPontos = this.senhaLetrasMinusculas.doMultiplicacaoPontos(this.senha, this.totalPontos);

		this.totalPontos = this.senhaLetrasMaisculas.doMultiplicacaoPontos(this.senha, this.totalPontos);
	}

	/*
	 * Realiza a adi��o de pontos de acordo com o tipo de caracter
	 */
	private void doAdicaoPontos() {

		for (int i = 0; i < this.arrSenha.length; i++) {
			if (this.arrSenha[i].matches(SenhaConstantes.MAISCULAS_A_Z)) {
				this.senhaLetrasMaisculas.doAdicaoPontos(this.arrSenha, i);
			} else if (this.arrSenha[i].matches(SenhaConstantes.MINUSCULAS_A_Z)) {
				this.senhaLetrasMinusculas.doAdicaoPontos(this.arrSenha, i);
			} else if (this.arrSenha[i].matches(SenhaConstantes.NUMEROS_0_9)) {
				if (i > 0 && i < (this.arrSenha.length - 1)) {
					this.senhaRepetidosRequiridos.setContMediaCaracteresRequiridos(this.senhaRepetidosRequiridos.getContMediaCaracteresRequiridos() + 1);
				}
				this.senhaNumeros.doAdicaoPontos(this.arrSenha, i);
			} else if (this.arrSenha[i].matches(SenhaConstantes.NUM_LETRAS_A_Z_A_Z0_9)) {
				if (i > 0 && i < (this.arrSenha.length - 1)) {
					this.senhaRepetidosRequiridos.setContMediaCaracteresRequiridos(this.senhaRepetidosRequiridos.getContMediaCaracteresRequiridos() + 1);
				}
				this.senhaSimbolos.doAdicaoPontos(this.arrSenha, i);
			}

			doContRepeatCaracteres(i);
		}
	}

	/*
	 * Realiza contagem do total de caracteres repetidos
	 */
	private void doContRepeatCaracteres(int index) {
		boolean flag = false;
		
		for (int i = 0; i < this.arrSenha.length; i++) {
			if (this.arrSenha[index].equals(this.arrSenha[i]) && index != i) {
				int contMediaInclusoesRepetidas = this.senhaRepetidosRequiridos.getContMediaInclusoesRepetidas() + Math.abs(this.arrSenha.length / (i - index));
				this.senhaRepetidosRequiridos.setContMediaInclusoesRepetidas(contMediaInclusoesRepetidas);
				flag = true;
			}
		}
		
		if (flag) {
			this.senhaRepetidosRequiridos.setContCaracteresRepeditos(this.senhaRepetidosRequiridos.getContCaracteresRepeditos() + 1);
			this.senhaRepetidosRequiridos.setContCaracteresUnicos(this.arrSenha.length - this.senhaRepetidosRequiridos.getContCaracteresRepeditos());
			
			if (this.senhaRepetidosRequiridos.getContCaracteresUnicos() > 0) {
				double contMediaInclusoesRepetidas = Math.ceil(this.senhaRepetidosRequiridos.getContMediaInclusoesRepetidas() / this.senhaRepetidosRequiridos.getContCaracteresUnicos());
				this.senhaRepetidosRequiridos.setContMediaInclusoesRepetidas(new Double(contMediaInclusoesRepetidas).intValue());
			} else  {
				this.senhaRepetidosRequiridos.setContMediaInclusoesRepetidas(new Double(Math.ceil(this.senhaRepetidosRequiridos.getContMediaInclusoesRepetidas())).intValue());
			}
		}
	}
}
