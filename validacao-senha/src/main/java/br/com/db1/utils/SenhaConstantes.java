package br.com.db1.utils;

public abstract class SenhaConstantes {
	
	private SenhaConstantes() {
		throw new UnsupportedOperationException();
	}
	
	public static final String NUM_LETRAS_A_Z_A_Z0_9 = "[^a-zA-Z0-9_]";
	
	public static final String NUMEROS_0_9 = "[0-9]";
	
	public static final String MAISCULAS_A_Z = "[A-Z]";
	
	public static final String MINUSCULAS_A_Z = "[a-z]";
	
	public static final String NUMEROS = "01234567890";
	
	public static String SIMBOLOS = ")!@#$%^&*()";
	
	public static String LETRAS = "abcdefghijklmnopqrstuvwxyz";
	
	public static int MINIMO_CARACTERES_SENHA = 8;
}
