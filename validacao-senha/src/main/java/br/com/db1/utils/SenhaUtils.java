package br.com.db1.utils;

public abstract class SenhaUtils {
	
	private SenhaUtils() {
		throw new UnsupportedOperationException();
	}
	
	public static String[] splitSenha(String senha) {
		String[] retorno = new String[senha.length()];

		for (int i = 0; i < senha.length(); i++) {
			retorno[i] = String.valueOf(senha.charAt(i));
		}
		return retorno;
	}
}
