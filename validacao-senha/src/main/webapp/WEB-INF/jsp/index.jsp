<!doctype html>
<html lang="pt-br">
  	<head>

	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		
		<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
		
	    <title>Valida��o Senha</title>
 	 </head>
 	 
  	<body>
		<div class="container-fluid padd-top" id="aplicacao">
			<div class="container">
		    	<div class="row">
		        	<div class="col-lg-6 no-padd">
		       			<p class="text-black-50 font-weight-bold">AVALIADOR DE SEGURAN�A DE SENHA</p>
					</div>
		        </div>
		         <div class="row row-1">
		
		        	<div class="col-lg-6">
		                <form>
		                    <input type="password" class="form-control" id="senha" placeholder="Senha">
		                </form>
		                
		                <div class="padd-top d-inline-flex w-100" id="resultado">
		                    <div class="badge senha-chumbo" id="contador"><span>0%</span></div>  
		                    <div class="badge senha-curta marg-left" id="descricao"><span>Muito Curta</span></div>                         
		                </div>
		            </div>
		
		        </div>
		    </div>
		</div>
	</body>
	<script>
		$(document).ready(function(){

			// fun��es
			function atualizaClasseSenha(classAdd) {
				var arrClasseSenha = ['senha-curta', 'senha-fraca', 'senha-boa', "senha-forte", "senha-muito-forte"];
				var arrTextoSenha = ['Muito curta', 'Fraca', 'Boa', "Forte", "Muito Forte"];

				$('#descricao').addClass(classAdd);
				
				for (var i = 0; i < 5; i++) {
					if (arrClasseSenha[i] != classAdd) {
						$('#descricao span').removeClass('classAdd');
					} else {
						$('#descricao span').text(arrTextoSenha[i]);
					}
				}
			}
			
			function atualizaPorcentagem(pontos) {
				
				if (pontos > 100) {
					pontos = 100;
				} else if (pontos < 0) {
					pontos = 0;
				}
				$('#contador span').text(pontos+'%');
				
				if (pontos >= 0 && pontos < 20) {
					atualizaClasseSenha("senha-curta");
				} else if (pontos >= 20 && pontos < 40) {
					atualizaClasseSenha("senha-fraca");
				} else if (pontos >= 40 && pontos < 60) {
					atualizaClasseSenha("senha-boa");
				} else if (pontos >= 60 && pontos < 80) {
					atualizaClasseSenha("senha-forte");
				} else if (pontos >= 80 && pontos <= 100) {
					atualizaClasseSenha("senha-muito-forte");
				}
			}

			function ajaxRequestValidacaoSenha(senha) {
				
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url :"/api/senha/valida",
					data: JSON.stringify({'senha':senha }),
					dataType : 'json',
					cache : false,
					timeout : 600000,
					success : function(pontos) {
						
						var valor = '-' + (pontos.data * 700 / 100) + 'px center';

						$('#contador').css('background-position', valor);

						atualizaPorcentagem(pontos.data);
					},
					error : function(e) {
						console.log("ERROR : ", e);
					}
				});
			}

			// eventos
			$("#senha").keyup(function(event) {

				var senha = $(this).val();

				ajaxRequestValidacaoSenha(senha);
			});
		});
	</script>
	<style>
		.body {
			background-color: #e5e5e5;
		}
		
		.padd-top {
			padding-top: 1rem;
		}
		
		.no-padd {
			padding: 0;
		}
		
		.marg-left {
			margin-left: 0.5rem;
		}
		
		.back-000 {
			background-color: #000;
		}
		
		.progress {
			height: 26px;
		}
		
		.progress-bar {
			height: 26px;
			transition: ease-in 0.2s;
			border-radius: 3px;
		}
		
		#resultado div span {
			color: #fff !important;
		}
		
		#aplicacao .row-1 {
			padding-top: 2rem;
			padding-bottom: 2rem;
			padding-left: 3rem;
			background-color: #e5e5e5;
			border-radius: 3px;
			border-top: solid 2px #dcdcdc;
		}
		
		#resultado div {
			line-height: 20px !important;
		}
		
		#resultado #contador {
			width: 20%;
			background-image:
				url('http://www.passwordmeter.com/images/bg_strength_gradient.jpg');
			background-repeat: no-repeat;
			background-position: 0px center;
			background-size: auto 100%;
		}
		
		.senha-chumbo {
			background-color: #ABABAB;
		}
		
		.text-chumbo {
			color: #9e9e9e;
		}
		
		.senha-muito-forte {
			background-color: #9e9e9e !important;
		}
		
		.senha-boa {
			background-color: #b6e629 !important;
		}
		
		.senha-forte {
			background-color: #f9cb1e !important;
		}
		
		.senha-fraca {
			background-color: #FF9800 !important;
		}
		
		.senha-curta {
			background-color: #E12125 !important;
		}
	</style>
</html>